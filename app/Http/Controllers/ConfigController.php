<?php

namespace App\Http\Controllers;

use App\Models\Config;
use Illuminate\Http\Request;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;
use Spatie\RouteAttributes\Attributes\Put;
use Spatie\RouteAttributes\Attributes\Delete;
use Spatie\RouteAttributes\Attributes\Prefix;
use Storage;

#[Prefix('configs')]
class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    #[Get('/', name: 'configs.index')]
    public function index()
    {
        $configs = Config::all();

        return view('configs.index', compact('configs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    #[Get('create', name: 'configs.create')]
    public function create()
    {
        return view('configs.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    #[Post('create', name: 'configs.store')]
    public function store(Request $request)
    {
        $config = Config::create(['title' => $request->title, 'data' => $request->data, 'password' => md5($request->password)]);
        Storage::disk('tpls')->put($config->id.'.blade.php', $config->data, 'public');
        return redirect()->route('configs.index');
    }

    /**
     * Display the specified resource.
     */
    #[Get('/{config}', name: 'configs.show')]
    public function show(Config $config)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    #[Get('/{config}/edit', name: 'configs.edit')]
    public function edit(Config $config)
    {
        return view('configs.edit', compact('config'));
    }

    /**
     * Update the specified resource in storage.
     */
    #[Put('/{config}/update', name: 'configs.update')]
    public function update(Request $request, Config $config)
    {

        if(md5($request->password) !== $config->password){
            return redirect()->route('configs.edit', $config)->with('password', 'Error password')->withInput();
        }   
        $config->update(['title' => $request->title, 'data' => $request->data, 'user_id' => 1]);
        Storage::disk('tpls')->put($config->id.'.blade.php', $config->data, 'public');
        return redirect()->route('configs.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    #[Delete('/{config}/destroy', name: 'configs.destroy')]
    public function destroy(Request $request, Config $config)
    {
        if(md5($request->password) !== $config->password){
            return redirect()->route('configs.index')->with('password', 'Error password');
        }
        if($config->forms->count()){
            return redirect()->route('configs.index')->with('relation', 'Error relation');
        }
        $config->delete();
        Storage::disk('tpls')->delete($config->id.'.blade.php');
        return redirect()->route('configs.index');
    }
}

