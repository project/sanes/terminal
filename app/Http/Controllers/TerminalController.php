<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\RouteAttributes\Attributes\Post;

class TerminalController extends Controller
{
    /**
     * Handle the incoming request.
     */
    #[Post('make-terminal', name: 'make-terminal')]
    public function __invoke(Request $request)
    {
        // dd($request);
        return response()->view('configs.data.'.$request->config, ['data' => $request])->withHeaders([
            'Content-Type' => 'text/html; charset=utf-8',
            'Content-Disposition' => 'attachment; filename="application.conf"'
        ]);
    }
}
