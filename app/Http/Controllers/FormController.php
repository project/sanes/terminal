<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\Config;
use Illuminate\Http\Request;;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;
use Spatie\RouteAttributes\Attributes\Put;
use Spatie\RouteAttributes\Attributes\Delete;
use Symfony\Component\Yaml\Yaml;
use Spatie\RouteAttributes\Attributes\Prefix;

#[Prefix('forms')]
class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    #[Get('/', name: 'forms.index')]
    public function index()
    {
        $forms = Form::all();
        return view('forms.index', compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     */
    #[Get('create', name: 'forms.create')]
    public function create()
    {
        $configs = Config::all();
        return view('forms.create', compact('configs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    #[Post('create', name: 'forms.store')]
    public function store(Request $request)
    {
        $form = Form::create(['title' => $request->title, 'data' => $request->data, 'config_id' => $request->config, 'password' => md5($request->password)]);
        return redirect()->route('forms.show', $form);
    }

    /**
     * Display the specified resource.
     */
    #[Get('/{form}', name: 'forms.show')]
    public function show(Form $form)
    {
        $data = Yaml::parse($form->data);
        return view('forms.show', compact(['data', 'form']));
    }

    /**
     * Show the form for editing the specified resource.
     */
    #[Get('/{form}/edit', name: 'forms.edit')]
    public function edit(Form $form)
    {
        $configs = Config::all();
        return view('forms.edit', compact(['form', 'configs']));
    }

    /**
     * Update the specified resource in storage.
     */
    #[Put('/{form}/update', name: 'forms.update')]
    public function update(Request $request, Form $form)
    {
        if(md5($request->password) !== $form->password){
            return redirect()->route('forms.edit', $form)->with('password', 'Error password')->withInput();
        }        
        $form->update(['title' => $request->title, 'config_id' => $request->config, 'data' => $request->data]);
        return redirect()->route('forms.show', $form);
    }

    /**
     * Remove the specified resource from storage.
     */
    #[Delete('/{form}/destroy', name: 'forms.destroy')]
    public function destroy(Request $request, Form $form)
    {

        if(md5($request->password) !== $form->password){
            return redirect()->route('forms.index')->with('password', 'Error password');
        }
        $form->delete();
        return redirect()->route('forms.index');
    }
}
