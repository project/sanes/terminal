<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'password', 'data'];

    public function forms()
    {
        return $this->hasMany(\App\Models\Form::class);
    }
}
