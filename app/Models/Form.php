<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'password', 'data', 'config_id'];


    public function config()
    {
        return $this->belongsTo(\App\Models\Config::class);
    }
    
}
