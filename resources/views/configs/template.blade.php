include "appdefaults"
platform5 {

  preventSecondLaunch : true
  frontOnly : "@{{ $data->front ?? 'true' }}"

  printing {
    dpi : 300
    enabled : true //печать вкл./выкл.

    devices : [
      {
         index : 1
         name : "@{{ $data->printer1 ?? 'Kyocera ECOSYS P6230cdn KX'}}" //имя принтера для печати, взять из лога при запуске
         page : {
          orientation : "portrait" // для всех беджей вертикальной ориентации
          swapSides : false // поменять местами длину и ширину листа // для всех беджей вертикальной ориентации
          scaling : "ACTUAL_SIZE"
                            // ACTUAL_SIZE,
                            // SHRINK_TO_FIT,
                            // STRETCH_TO_FIT,
                            // SCALE_TO_FIT

        
        }
      }
      {
         index : 2
         name : "@{{ $data->printer2 ?? ''}}" //имя принтера для печати, взять из лога при запуске
         page : {
          orientation : "portrait" //для горизонталой ориентации (карты, ебанутый формат бумаги)
          swapSides : false // поменять местами длину и ширину листа //для горизонталой ориентации (карты, ебанутый формат бумаги)
          scaling : "ACTUAL_SIZE"
                            // ACTUAL_SIZE,
                            // SHRINK_TO_FIT,
                            // STRETCH_TO_FIT,
                            // SCALE_TO_FIT

        }
      }
    ]
  }

  //настройка подключения к основному серверу platform5
  server.remote {
    enabled : true
    system : "platform5-system"
    //здесь поменять на адрес сервера
    host : "@{{ $data->serverWs ?? '' }}"
    //хост админки
    webHost : "@{{ $data->serverHttp ?? '' }}"
  }

  terminal {

    accessControlEnabled : true //сканер вкл./выкл.

    //параметры терминала для печати и кд
    autoLogin {
      enabled : true
      manuallyUserName : false //вводить логин и пароль фронта вручную
      username : "@{{ $data->username ?? '' }}"
      password : "@{{ $data->password ?? '' }}"
      terminal : "@{{ $data->terminal ?? ''}}"
      externalId : "extidtest"
      monitors : [
        { name : "1", position : "0" }
      ]
    }

    config {
      //список ком портов для подключения сканеров
      devices : [{
        enabled : false
        name : "emulator"
        driverType : "emulator"
        parameters : {
          delay : 5000
          data : ["C58274C99000"]
        },
        tag : "enter"
      },
        {
          enabled : true
          name : "serialScanner"
          driverType : "serialport"
          parameters {
            port : "COM@{{ $data->comPort ?? '3' }}"
            speed : 9600
          }
        },
        //считыватель UHF rfid меток RRU9890
        {
          enabled : false
          name : "rfid9809"
          driverType : "rfid9809"
          parameters {
            port : 5
            delay : 100 //интервал опроса карт, мс
            writeTimeout : 1000 //таймаут записси EPC, мс
          }
        },
        //Omnikey
        {
          enabled : false
          name : "omnikey"
          driverType : "omnikey"
          parameters {
            delay :10000
            portName : "OMNIKEY CardMan 5x21-CL 0"
          }
        },
      ]

      //формат обраотки данных сканера
      formats : [{
        name : "testFormat"
        driverType : "plain",
        template : "$data$",
        parameters : {
          regexp: "^([A-z0-9]{80}).+$" //вырезает первые 8 символов и отдает их, состоящих из латинских букв A-Z или цифр 0-9
          // ^([A-z0-9]+).{4}$ //отрезает последние 4 символа и отдает начало строки
        }
      }
      ]

      //тип реакции на сканирование
      //      "redirect" ⇒ открыть анкету в админке(только при коннекте админки к терминалу)
      //      "print" ⇒ печать
      //      "check_badge" ⇒ контроль доступа
      //      "assign_barcode" ⇒ назначить рфид
      //      "open_and_assign" :> открыть анкету и назначить рфид
      reactions : [{
        name : "Redirect"
        type : "redirect"
        parameters : {
          badgeTypeId : 2
        }
      },
        {
          name : "AssignBarcode"
          type : "assign_barcode"
          parameters : {
          }
        },
        {
          name : "CheckBadge"
          type : "check_badge"
          parameters : {
          }
        },
        {
          name : "OpenAndAssign"
          type : "open_and_assign"
          parameters : {
          }
        }
      ]

      tripod : {
        enabled : false
        directionEnter : "enter"
        directionExit : "exit"
        direction : "Exit"
        port : "2" // Ищем при старте терминала строки
                    // [INFO ] [sevts.terminal.tripod.TripodController] -  Scan tripod ports:
                    //::: Scan completed :::
                    // подставляем порядковый номер порта из списка, отсчет с НУЛЯ.

        ENTER_ALWAYS : "111",
        EXIT_ALWAYS : "222",
        TWO_WAY : "333",
        CLOSE : "444",
        BLOCK : "555"
      }

      usbRelay : {
        enabled : false
        directionEnterTag : "enter"
        directionExitTag : "exit"

        relaySerial : "xxx"
        enterChannelNum : 1
        exitChannelNum : 2

        closeTime : 1s

        dllPath : "F:/work/platform5-terminal/dll"

      }

      //конфигурация сканеров
      scanners : [
        {
          name : "test1"
          tag : "enter"
          device : "serialScanner" //имя из списка devices
          format : "testFormat" // формат из списка formats
          reaction : "OpenAndAssign" // реакция из списка reactions
          //дополнительные параметры
          parameters : {
            dataField : "72" //ид поля для поиска
            formId : "2"
            formList : ["49", "60"] //ид формы: ид поля
            badgeSearch : false //поиск по бейджу
          }
        },
        {
          name : "RFIDWriter"
          device : "rfid9809"
          format : "plainFormat" // формат из списка formats
          reaction : "AssignBarcode" // реакция из списка reactions

          //дополнительные параметры
          parameters : {
            dataField : "1" //ид поля для записи TID
            formId : "1" //ид формы
            badgeSearch : true //поиск по бейджу
            formList : ["49", "60"] //список ид полей, по всем формам
          }
        },
        {
          name : "Omnikey"
      tag : "enter"
          device : "omnikey"
          format : "testFormat" // формат из списка formats
          reaction : "OpenAndAssign" // реакция из списка reactions
      

          //дополнительные параметры
          parameters : {
            dataField : "1" //ид поля для записи TID
            formId : "1" //ид формы
            badgeSearch : false //поиск по бейджу
      formList : ["49", "60"]

          }
        }
      ]
    }
  }
}
