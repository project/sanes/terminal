@extends('layouts.base')
@section('content')
<h3 class="">Добавить конфигурацию</h3>
<form action="{{ route('configs.store') }}" class="uk-form-stacked" method="post">
	@csrf
	<div class="uk-child-width-1-2@m" uk-grid>
		<div>
			<div class="uk-form-controls">
				<label for="title" class="uk-form-label">Имя конфигурации</label>
				<input type="text" class="uk-input" id="title" name="title" required minlength="5">
			</div>
		</div>
		<div>
			<div class="uk-form-controls">
				<label for="password" class="uk-form-label">Пароль</label>
				<input type="password" class="uk-input" id="password" name="password" required minlength="4">
			</div>
		</div>
	</div>
	<div class="uk-margin">
		<div class="uk-form-controls">
			<label for="data" class="uk-form-label">Конфигурация</label>
			<textarea name="data" id="data" class="uk-textarea uk-text-small" required rows="20">@include('configs.template')</textarea>
		</div>
	</div>
	<div class="uk-margin">
		<button class="uk-button uk-button-primary" type="submit">Добавить</button>
		<a href="{{ route('configs.index') }}" class="uk-button uk-button-default">Отмена</a>
	</div>
</form>
<hr class="uk-margin-large">
@endsection
@section('js')
<script src="/js/yaml.js"></script>
<script>
  var editor = CodeMirror.fromTextArea(document.getElementById("data"), {
    lineNumbers: true,
    styleActiveLine: true,
    matchBrackets: true,
    mode:  "yaml",
    keyMap: "sublime",
    extraKeys: {
      "F11": function(cm) {
        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
      },
      "Esc": function(cm) {
        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
      }
    }
  });	
</script>
@endsection