@extends('layouts.base')
@section('content')
<h3 class="">Изменить конфигурацию</h3>
@if(Session::has('password'))
<p class="uk-text-danger">Ошибка пароля</p>
@endif
<form action="{{ route('configs.update', $config) }}" class="uk-form-stacked" method="post">
	@csrf
	@method('put')
	<div class="uk-margin">
		<div>
			<div class="uk-form-controls">
				<label for="title" class="uk-form-label">Имя формы</label>
				<input type="text" class="uk-input" id="title" name="title"  value="{{ old('title', $config->title) }}" required minlength="5">
			</div>
		</div>
	</div>
	<div class="uk-margin">
		<div class="uk-form-controls">
			<label for="data" class="uk-form-label">Конфигурация</label>
			<textarea name="data" id="data" class="uk-textarea" rows="10" required>{{ old('data', $config->data) }}</textarea>
		</div>
	</div>


	<div class="" uk-grid>
		<div class="uk-width-expand">
			<div class="uk-form-controls">
				<input type="password" class="uk-input" id="password" name="password" required minlength="4">
			</div>
		</div>		
		<div class="uk-width-auto@m">
			<button class="uk-button uk-button-primary" type="submit">Сохранить</button>
			<a href="{{ route('configs.show', $config) }}" class="uk-button uk-button-default">Отмена</a>
		</div>
	</div>
	<hr class="uk-margin-large">
</form>
@endsection
@section('js')
<script src="/js/yaml.js"></script>
<script>
  var editor = CodeMirror.fromTextArea(document.getElementById("data"), {
    lineNumbers: true,
    styleActiveLine: true,
    matchBrackets: true,
    mode:  "yaml",
    keyMap: "sublime",
    extraKeys: {
      "F11": function(cm) {
        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
      },
      "Esc": function(cm) {
        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
      }
    }    
  });	
</script>
@endsection