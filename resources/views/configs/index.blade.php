@extends('layouts.base')
@section('content')
	<div uk-grid>
		<div class="uk-width-expand">
			<h3>Конфигурации</h3>
		</div>
		<div class="uk-width-auto">
			<a href="{{ route('configs.create') }}" class="uk-icon uk-link-reset" uk-icon="plus"></a>
		</div>
	</div>
	@if(Session::has('password'))
	<p class="uk-text-danger">Ошибка пароля</p>
	@elseif(Session::has('relation'))
	<p class="uk-text-danger">Есть формы с этой конфигурацией</p>
	@endif
	<table class="uk-table uk-table-middle uk-table-striped uk-table-hover">
		<thead>
			<tr>
				<th>Конфигурация</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($configs as $config)
			<tr>
				<td class="uk-table-link uk-width-auto"><a href="{{ route('configs.edit', $config) }}" class="uk-link-reset">{{ $config->title }}</a></td>
				<td style="width: 50px;">
					<ul class="uk-iconnav">
						<li><a href="{{ route('configs.edit', $config) }}" class="uk-icon-link" uk-icon="file-edit"></a></li>
						<li><a onclick='destroy("{{ route('configs.destroy', $config) }}")' class="uk-icon-link" uk-icon="trash"></a></li>
					</ul>		
				</td>
			</tr>
			@endforeach
			<form id="destroy" class="uk-hidden" method="post">
				@csrf
				@method('delete')
				<input type="text" class="uk-hidden" name="password" id="password">
			</form>
		</tbody>
	</table>
@endsection
@section('js')
	<script>
		function destroy(url){
			var password = prompt('Pin Code')
			// alert(pin)
			var form = document.querySelector('#destroy')
			var code = document.querySelector('#password')
			form.action = url
			code.value = password
			if(confirm('Delete?')){
				form.submit()
			}						
		}
	</script>
@endsection