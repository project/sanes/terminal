@extends('layouts.base')
@section('content')
<h3 class="">Добавить форму</h3>
<form action="{{ route('forms.store') }}" class="uk-form-stacked" method="post">
	@csrf
	<div class="uk-margin">
		<div class="uk-form-controls">
			{{ html()->label('Имя формы', 'title')->class('uk-form-label') }}
			{{ html()->text('title')->minlength(5)->class(['uk-input'])->id('title')->required(true) }}			
		</div>
	</div>
	<div class="uk-margin">
		<div class="uk-form-controls">
		{{ html()->label('Конфигурация', 'config')->class('uk-form-label') }}
		{{ html()->select('config')->class('uk-select')->required()->open() }}
		@foreach($configs as $config)
		{{ html()->option($config->title, $config->id) }}
		@endforeach
		{{ html()->select('config')->close() }}
		</div>
	</div>
	<div class="uk-margin">
		<div class="uk-form-controls">
			{{ html()->label('Пароль', 'password')->class('uk-form-label') }}
			{{ html()->password('password')->minlength(4)->class(['uk-input'])->id('password')->required(true) }}
		</div>
	</div>
	<div class="uk-margin">
		<div class="uk-form-controls">
			{{ html()->label('Конфигурация', 'data')->class('uk-form-label') }}
			{{ html()->textarea('data')->class('uk-textarea')->id('data')->open() }}@include('forms.template'){{ html()->textarea('data')->close() }}
		</div>
	</div>	
	<div class="uk-margin">
		<button class="uk-button uk-button-primary" type="submit">Добавить</button>
		<a href="{{ route('forms.index') }}" class="uk-button uk-button-default">Отмена</a>
	</div>
	<hr class="uk-margin-large">
</form>
@endsection
@section('js')
<script src="/js/yaml.js"></script>
<script>
  var editor = CodeMirror.fromTextArea(document.getElementById("data"), {
    lineNumbers: true,
    styleActiveLine: true,
    matchBrackets: true,
    mode:  "yaml",
    keyMap: "sublime",
    extraKeys: {
      "F11": function(cm) {
        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
      },
      "Esc": function(cm) {
        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
      }
    }
  });	
</script>
@endsection