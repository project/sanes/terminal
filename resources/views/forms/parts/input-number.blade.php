{{ html()
	->number($field['name'], $field['value'] ?? false, $field['min'] ?? '', $field['max'] ?? '')
	->id($field['name'])
	->required($field['required'] ?? true)
	->class('uk-input')
	->disabled(!empty($field['disabled']) )
}}