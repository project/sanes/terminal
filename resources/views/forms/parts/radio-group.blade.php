<div class="uk-child-width-1-2@m" uk-grid>
	
@foreach($field['radio'] as $radio)
<label for="{{ $radio['id'] }}" style="cursor: pointer;">
    <input type="radio" 
    class="uk-radio" 
    id="{{ $radio['id'] }}" 
    name="{{ $field['name'] }}" 
    value="{{ $radio['value'] }}" 
    @checked(!empty($radio['checked'])) 
    required> 
    {{ $radio['label'] }}
</label>
@endforeach
</div>