<div class="uk-margin">
	<div class="uk-form-controls">
	<label for="{{ $field['name'] }}" class="uk-form-label">{{ $field['label'] }}</label>
	@include('forms.parts.'.$field['component'])
	</div>
</div>