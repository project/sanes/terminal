{{ html()
	->text($field['name'])
	->id($field['name'])
	->required($field['required'] ?? true)
	->class(!isset($field['disabled']) ? 'uk-input' : 'uk-input uk-disabled')
	->value($field['value'] ?? false)
}}