<div class="uk-inline uk-width-expand">
	<a class="uk-form-icon uk-form-icon-flip" uk-icon="icon: eye" onclick="showPassword('{{ $field['name'] }}')"></a>
	{{ html()
		->password($field['name'])
		->id($field['name'])
		->required($field['required'] ?? true)
		->class(!isset($field['disabled']) ? 'uk-input' : 'uk-input uk-disabled') 
		->value($field['value'] ?? false)
	}}
</div>