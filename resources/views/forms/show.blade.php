@extends('layouts.base')
@section('content')
<div class="uk-margin" uk-grid>
	<div class="uk-width-expand">
		<h3 class="">{{ $form->title }}</h3>
<p class="uk-margin-small uk-flex uk-flex-middle"> <span class="uk-margin-small-right" uk-icon="cog"></span>{{ $form->config->title }} ({{ $form->config->updated_at->format('d-M H:i') }})</p>	
	</div>
	<div class="uk-width-auto">
		<a href="{{ route('forms.edit', $form) }}" class="uk-icon uk-link-reset" uk-icon="file-edit"></a>
	</div>
</div>
{{ html()->form($data['method'] , route($data['action']))->class('uk-form-stacked')->open() }}
	<input type="text" class="uk-hidden" value="{{ $form->config_id }}" name="config">
	<div class="uk-child-width-1-2@m uk-grid-medium" uk-height-match="target: > div > div > fieldset > .uk-margin" uk-grid="masonry: pack">
		@foreach($data['fieldsets'] as $fieldset)
		<div>
			<div class="uk-background-muted uk-padding-small">
				<fieldset class="uk-fieldset">
					<legend class="uk-legend">{{ $fieldset['legend'] }}</legend>
					@foreach($fieldset['fields'] as $field)
						@include('forms.parts.field')
					@endforeach
				</fieldset>
			</div>
		</div>
		@endforeach
	</div>
	<div class="uk-margin">
		<button class="uk-button uk-button-primary" type="submit">{{ $data['button'] }}</button>
		<a href="{{ route('forms.index') }}" class="uk-button uk-button-default">Cancel</a>
	</div>
	<hr class="uk-margin-large">
{{ html()->form()->close() }}

@endsection
@section('js')
<script>
	function showPassword(name){
		var field = document.querySelector("#" + name)
		if(field.type === "password"){
			field.type = "text"
			field.previousElementSibling.setAttribute("uk-icon", "eye-slash")
		}
		else {
			field.type = "password"
			field.previousElementSibling.setAttribute("uk-icon", "eye")
		}
		// document.querySelector(`[type="password"]`).type = 'text'
	}
</script>
@endsection