action: 'make-terminal'
method: 'post'
button: 'Make Terminal'
fieldsets:
  - legend: 'Сканер'
    fields: 
    - {component: 'input-number', name: 'comPort', label: 'Com port', value: '3', min: 1, max: 20}
  - legend: 'Принтеры'
    fields: 
    - {component: 'input-text', name: 'printer1', label: 'Принтер 1', value: 'Kyocera ECOSYS P6230cdn KX'}
    - {component: 'input-text', name: 'printer2', label: 'Принтер 2', value: 'Kyocera ECOSYS P6630cdn'}
  - legend: 'Терминал'
    fields: 
    - {component: 'input-text', name: 'serverHttp', label: 'Сервер HTTP', value: 'https://'} 
    - {component: 'input-text', name: 'serverWs', label: 'Сервер WS', value: 'wss://'} 
    - {component: 'input-text', name: 'terminal', label: 'Терминал', value: 'tm'}
    - {component: 'input-text', name: 'username', label: 'Логин', value: 'user', disabled: true}
    - {component: 'input-password', name: 'password', label: 'Пароль', placeholder: 'password', value: 'StrongPAssword'}
  - legend: 'Планшет'
    fields: 
      - component: 'radio-group'
        label: 'Front Only'
        name: 'front'
        radio:
        - {label: 'True', id: 'true', value: 'true'}
        - {label: 'False', id: 'false', value: 'false'}