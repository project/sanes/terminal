@extends('layouts.base')
@section('content')
	<div uk-grid>
		<div class="uk-width-expand">
			<h3>Формы</h3>
		</div>
		<div class="uk-width-auto">
			<a href="{{ route('forms.create') }}" class="uk-icon uk-link-reset" uk-icon="plus"></a>
		</div>
	</div>
	@if(Session::has('password'))
	<p class="uk-text-danger">Ошибка пароля</p>
	@endif
	<table class="uk-table uk-table-middle uk-table-striped uk-table-hover uk-text-small">
		<thead>
			<tr>
				<th>Форма</th>
				<th>Добавлена</th>
				<th>Обновлена</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($forms as $form)
			<tr>
				<td class="uk-table-link uk-width-expand"><a href="{{ route('forms.show', $form) }}" class="uk-link-reset">{{ $form->title }}</a></td>
				<td class="uk-text-nowrap">{{ \Carbon\Carbon::parse($form->created_at)->format('d-M H:i') }}</td>
				<td class="uk-text-nowrap">{{ \Carbon\Carbon::now(config('app.timezone'))->longAbsoluteDiffForHumans($form->updated_at); }}</td>
				<td>
					<ul class="uk-iconnav" style="width: 60px;">
						<li><a href="{{ route('forms.edit', $form) }}" class="uk-icon-link" uk-icon="file-edit"></a></li>
						<li><a onclick='destroy("{{ route('forms.destroy', $form) }}")' class="uk-icon-link" uk-icon="trash"></a></li>
					</ul>		
				</td>
			</tr>
			@endforeach
			<form id="destroy" class="uk-hidden" method="post">
				@csrf
				@method('delete')
				<input type="text" class="uk-hidden" name="password" id="password">
			</form>
		</tbody>
	</table>
@endsection
@section('js')
	<script>
		function destroy(url){
			var password = prompt('Pin Code')
			var form = document.querySelector('#destroy')
			var code = document.querySelector('#password')
			form.action = url
			code.value = password
			if(confirm('Delete?')){
				form.submit()
			}						
		}
	</script>
@endsection
