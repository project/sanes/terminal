<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title ?? config('app.name') }}</title>
    <link rel="icon" href="/favicon.png">
    <link rel="stylesheet" href="/css/uikit.min.css">
    <link rel="stylesheet" href="/css/codemirror.css">
    <link rel="stylesheet" href="/css/fullscreen.css">
    <script src="/js/uikit.min.js"></script>
    <script src="/js/uikit-icons.min.js"></script>
    <script src="/js/codemirror.js"></script>
    <script src="/js/sublime.js"></script>
    <script src="/js/fullscreen.js"></script>
    <style>
        .CodeMirror {
          border: 1px solid #eee;
          height: auto;
        }       
        .uk-form-label {
            cursor: pointer;
        }
    </style>

</head>
<body>
    <div class="uk-container uk-container-small">
        <div class="uk-navbar navbar" uk-navbar uk-inverse>
            <div class="uk-navbar-left">
                <a href="/" class="uk-logo uk-text-uppercase uk-navbar-item">{{ config('app.name') }}</a>
            </div>
            <div class="uk-navbar-right">
                <ul class="uk-navbar-nav" uk-inverse>
                    <li><a href="{{ route('forms.index') }}">Формы</a></li>
                    <li><a href="{{ route('configs.index') }}">Конфигурации</a></li>
                </ul>
            </div>
        </div>
        <hr class="uk-margin-remove-top">
        @yield('content')
    </div>
    @yield('js')
</body>
</html>